import java.math.BigInteger;
/**
 * ������� ������� ��� 5-�� ������������ �� ������ ����������<br/>
 * �������:<br/>
 * ����� ���������� ����� �� ������� ����������� � ������, ���� ��� ���� ������� �����������
 * @author ���� �������
 */
public class Safety{
	/** ����������� ����� */
	static BigInteger N = new BigInteger(
			"41532294675915846631035277088738593690201223273856011981647976673022145" +
			"88870635582693100909413952115961735627123436097545409717992063575" +
			"00060590437775050244070653829921526853693548663404123456292103778" +
			"8620014150375092399385273967413490630080326376508438534969408415057"
	);
	public static void main(String[] args) {
		/** ��������� �������� A = sqrt(N) + 1 */
		BigInteger A = sqrt(N).add(new BigInteger("1"));
		while(true) {   
	    	/** x = sqrt(A^2 - N)*/
	    	BigInteger x  = sqrt( A.pow(2).subtract(N));
		    /** p = A - x */
	        BigInteger p  = A.subtract(x);
	        /** q = A + x */
	        BigInteger q  = A.add(x);
	        /** qp = q * p */
	        BigInteger qp = p.multiply(q);
	        if(N.compareTo(qp)==0){ /** N = qp */
	        	/** �������� p � q �� �������� */
	        	if ((p.isProbablePrime(1)) && (q.isProbablePrime(1))){
	            	System.out.println("q  = "  + q);
	                System.out.println("p  = "  + p);
	                System.out.println("qp = " + qp);
	                System.out.println(" N = " +  N);
	                break;
	            }
	        }
	        /** ��������� � */
	        A = A.add(new BigInteger("1"));
	    }
	}
	/** ����� ����������� ���������� ������, ��� � BigInteger ����� ��� */
	static BigInteger sqrt(BigInteger n) {
		BigInteger a = BigInteger.ONE;
		BigInteger b = new BigInteger(n.shiftRight(5).add(new BigInteger("8")).toString());
		while(b.compareTo(a) >= 0) {
			BigInteger mid = new BigInteger(a.add(b).shiftRight(1).toString());
			if(mid.multiply(mid).compareTo(n) > 0) 
				b = mid.subtract(BigInteger.ONE);
			else 
				a = mid.add(BigInteger.ONE);
		}
		return a.subtract(BigInteger.ONE);
	}
}